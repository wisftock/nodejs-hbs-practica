const express = require('express');
const path = require('path');
const app = express();
const hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.PORT || 3000;

// app.use(express.static(__dirname + '/view'));
app.set('views', path.join(__dirname, './views'));
app.use(express.static('public'));

// express hbs
hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');

app.get('/', (req, res) => {
  res.render('home', { nombre: 'pedro' });
});
app.get('/about', (req, res) => {
  res.render('about');
});

app.listen(port, () => {
  console.log(`Escuchando peticiones en el puerto ${port}`);
});
